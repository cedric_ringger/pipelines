module.exports = {
  globals: {
    "ts-jest": {
      tsconfig: "tsconfig.json"
    }
  },
  preset: "ts-jest",
  testEnvironment: "node",
  roots: [],
  rootDir: "./src/tests",
  verbose: true,
  moduleFileExtensions: ["ts", "js"],
  "testMatch": ["<rootDir>/**/*.test.ts"]
}