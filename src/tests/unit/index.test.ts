import { convertToLowerCase } from "../../utils";

describe("index test", () => {
  test("convert to lowercase", () => {
    expect(convertToLowerCase("TEST")).toBe("test");
  });
});
